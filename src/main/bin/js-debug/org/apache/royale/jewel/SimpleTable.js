/**
 * Generated by Apache Royale Compiler from org/apache/royale/jewel/SimpleTable.as
 * org.apache.royale.jewel.SimpleTable
 *
 * @fileoverview
 *
 * @suppress {missingRequire|checkTypes|accessControls}
 */

goog.provide('org.apache.royale.jewel.SimpleTable');
/* Royale Dependency List: org.apache.royale.core.WrappedHTMLElement,org.apache.royale.html.util.addElementToWrapper*/

goog.require('org.apache.royale.jewel.Group');



/**
 *  constructor.
 *
 *  @langversion 3.0
 *  @playerversion Flash 10.2
 *  @playerversion AIR 2.6
 *  @productversion Royale 0.9.4
 * @constructor
 * @extends {org.apache.royale.jewel.Group}
 */
org.apache.royale.jewel.SimpleTable = function() {
  org.apache.royale.jewel.SimpleTable.base(this, 'constructor');
  this.typeNames = "jewel simpletable";
};
goog.inherits(org.apache.royale.jewel.SimpleTable, org.apache.royale.jewel.Group);


/**
 * Prevent renaming of class. Needed for reflection.
 */
goog.exportSymbol('org.apache.royale.jewel.SimpleTable', org.apache.royale.jewel.SimpleTable);


/**
 * @royaleignorecoercion org.apache.royale.core.WrappedHTMLElement
 * @protected
 * @override
 */
org.apache.royale.jewel.SimpleTable.prototype.createElement = function() {
  return org.apache.royale.html.util.addElementToWrapper(this, 'table');
};


/**
 * Metadata
 *
 * @type {Object.<string, Array.<Object>>}
 */
org.apache.royale.jewel.SimpleTable.prototype.ROYALE_CLASS_INFO = { names: [{ name: 'SimpleTable', qName: 'org.apache.royale.jewel.SimpleTable', kind: 'class' }] };



/**
 * Reflection
 *
 * @return {Object.<string, Function>}
 */
org.apache.royale.jewel.SimpleTable.prototype.ROYALE_REFLECTION_INFO = function () {
  return {
    methods: function () {
      return {
        'SimpleTable': { type: '', declaredBy: 'org.apache.royale.jewel.SimpleTable'}
      };
    }
  };
};
/**
 * @const
 * @type {number}
 */
org.apache.royale.jewel.SimpleTable.prototype.ROYALE_COMPILE_FLAGS = 10;
